CREATE TABLE rrhh.usuarios
(
    id serial primary key,
    usuario text NOT NULL,
    correo text NOT NULL UNIQUE, 
    contrasena text NOT NULL,
    activo boolean DEFAULT true,
    CONSTRAINT unq_usuarios_usuario UNIQUE (usuario)
);

select * from rrhh.usuarios
update rrhh.usuarios set contrasena = ? where id = ? 


create table rrhh.imagenes(
	id serial primary key, 
	id_usuario int not null, 
	imagen bytea,
	constraint fk_ima_usu foreign key(id_usuario) references rrhh.usuarios(id)
);

select id_usuario, imagen from rrhh.imagenes
-- alter table rrhh.usuarios add column id_imagen int
-- alter table rrhh.usuarios add constraint fk_usu_ima foreign key(id_imagen) 
-- references rrhh.imagenes(id)
-- alter table rrhh.usuarios drop column id_imagen
delete from rrhh.usuarios where id = 11

select ta.id, ta.usuario, ta.correo, tb.imagen from rrhh.usuarios ta inner join rrhh.imagenes tb 
on ta.id = tb.id_usuario


CREATE TABLE rrhh.empleados(
	id serial primary key,
	cedula text not null unique,
	nombre text not null,
	apellido_uno text not null,
	apellido_dos text,
	telefono text not null,
	genero char default 'M',
	estado_civil char default 'S',
	direccion text, 
	activo boolean default false
);


insert into rrhh.empleados(cedula, nombre, apellido_uno, apellido_dos, telefono, genero, estado_civil, direccion, activo) 
values ('206470762', 'Allan', 'Murillo', 'Alfaro', '8526-2638', 'M', 'C', 'Chachagua',true)

select * from rrhh.empleados

select id,cedula, nombre, apellido_uno, apellido_dos, telefono, genero, estado_civil, direccion, activo from rrhh.empleados 
 where lower(cedula) like lower('mor%') or lower(apellido_uno) like lower('mor%') order by apellido_uno

create table rrhh.areas (
    id serial primary key,
    codigo text not null unique, 
    nombre text not null unique,
    activo boolean default true
);

create table rrhh.puestos (
    id serial primary key,
    id_area int not null,
    codigo text not null, 
    nombre text not null,
    salario numeric default 0,
    activo boolean default true,
    constraint fk_pue_are foreign key(id_area) references rrhh.areas(id), 
    constraint unq_codare unique(codigo, id_area)
)
-- insert into rrhh.areas (codigo, nombre, activo) values('CONT','Contabilidad', true)
-- insert into rrhh.puestos(id_area, codigo, nombre,salario) values(3,'CO-ASI', 'Asistente', 250000)
-- select * from rrhh.puestos
create table rrhh.contratos(
    id serial primary key,
    id_empleado int not null, 
    id_puesto int not null, 
    fecha date not null, 
    salario numeric default 0, 
    activo boolean default true,
    constraint fk_con_emp foreign key(id_empleado) references rrhh.empleados(id), 
    constraint fk_con_pue foreign key(id_puesto) references rrhh.puestos(id)
)

alter table rrhh.usuarios add column id_empleado int
alter table rrhh.usuarios add constraint fk_usu_emp foreign key(id_empleado) references  rrhh.empleados(id)
alter table rrhh.usuarios add constraint unq_usuemp unique(id_empleado)
select * from rrhh.contratos

insert into rrhh.contratos(id_empleado, id_puesto, fecha, salario, activo) 
values (?,?,?,?,?)

select  id, id_puesto, fecha, salario, activo from rrhh.contratos where id_empleado = 3 order by fecha desc limit 1

drop table  rrhh.vacaciones

create table rrhh.vacaciones(
    id serial primary key, 
    id_empleado int ,
    motivo text not null, 
    fecha_ini date not null, 
    fecha_fin date not null, 
    dias int default 0,
    observacion text default '',
    estado char default 'S', 
    constraint fk_vac_emp foreign key(id_empleado) references rrhh.empleados(id),
    constraint chk_vac_est check(estado in ('S', 'R', 'A'))
)

select id, id_empleado, motivo, fecha_ini, fecha_fin, dias, observacion, estado from rrhh.vacaciones where id_empleado = ?

insert into rrhh.vacaciones(id_empleado, motivo, fecha_ini, fecha_fin) 
values(1,'Personal','2020-04-20','2020-04-24')

update rrhh.vacaciones set dias = 0, estado = 'R', observacion='Solicitud duplicada' where id = 2



select u.*, (a.id=1) as admin from rrhh.usuarios u 
inner join rrhh.contratos c on u.id_empleado = c.id_empleado
inner join rrhh.puestos p on c.id_puesto = p.id
inner join rrhh.areas a on p.id_area = a.id
where u.activo and u.contrasena = '25d55ad283aa400af464c76d713c07ad' and (usuario = 'elias' or correo = 'elias')
order by c.fecha desc limit 1

select * from rrhh.contratos 
delete from rrhh.contratos where id = 4


select e.id,e.cedula, e.nombre, e.apellido_uno, e.apellido_dos, e.telefono, 
e.genero, e.estado_civil, e.direccion, e.activo 
from rrhh.empleados e 
inner join rrhh.usuarios u on e.id = u.id_empleado
where u.id = ?

update rrhh.contratos set fecha = '2018-04-18' where id = 3

-- alter table rrhh.vacaciones alter column id_empleado drop not null;
select * from rrhh.vacaciones

delete from rrhh.vacaciones where id = 8



select u.*, (a.id=1) as admin from rrhh.usuarios u 
inner join rrhh.contratos c on u.id_empleado = c.id_empleado
inner join rrhh.puestos p on c.id_puesto = p.id
inner join rrhh.areas a on p.id_area = a.id
where u.activo and u.contrasena = '25d55ad283aa400af464c76d713c07ad' and (usuario = 'lmatamoros' or correo = 'lmatamoros')
order by c.fecha desc limit 1

select * from rrhh.areas