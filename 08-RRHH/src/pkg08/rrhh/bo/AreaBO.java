/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg08.rrhh.bo;

import java.util.LinkedList;
import pkg08.rrhh.dao.AreaDAO;
import pkg08.rrhh.entities.Area;
import pkg08.rrhh.entities.Puesto;

/**
 *
 * @author amurilloa
 */
public class AreaBO {

    public LinkedList<Area> cargarA(boolean activas) {
        return new AreaDAO().cargarA(activas);
     }

    public LinkedList<Puesto> cargarP(Area a, boolean activos) {
        return new AreaDAO().cargarP(a,activos);
    }
    
}
