/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg08.rrhh.bo;

import pkg08.rrhh.dao.ContratoDAO;
import pkg08.rrhh.entities.Contrato;
import pkg08.rrhh.entities.Empleado;

/**
 *
 * @author amurilloa
 */
public class ContratoBO {

    public boolean insertar(Contrato c, Empleado emp) {

        if (c == null) {
            throw new RuntimeException("Debe crear un nuevo contrato");
        }
        if (emp == null) {
            throw new RuntimeException("Debe seleccionar un empleado");
        }

        if (c.getSalario() < c.getPuesto().getSalario()) {
            throw new RuntimeException("El salario del contrato no puede ser "
                    + "menor al salario ofertado en el puesto(" + c.getPuesto().getSalario() + ")");
        }
        
        return new  ContratoDAO().insertar(c, emp);

    }


}
