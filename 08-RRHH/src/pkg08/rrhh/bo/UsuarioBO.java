/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg08.rrhh.bo;

import java.util.LinkedList;
import org.apache.commons.codec.digest.DigestUtils;
import pkg08.rrhh.dao.UsuarioDAO;
import pkg08.rrhh.entities.Empleado;
import pkg08.rrhh.entities.Usuario;

/**
 *
 * @author amurilloa
 */
public class UsuarioBO {

    public boolean registrar(Usuario usuario, String repass) {

        if (usuario == null) {
            throw new RuntimeException("Debe crear un nuevo usuario");
        }

        if (usuario.getUsuario().isBlank()) {
            throw new RuntimeException("Usuario requerido");
        }

        if (usuario.getCorreo().isBlank()) {
            throw new RuntimeException("Correo requerido");
        }

        if (usuario.getId() <= 0) {
            if (usuario.getContrasena().length() < 8) {
                throw new RuntimeException("Contraseña con 8 caracteres como mínimo");
            }

            if (!usuario.getContrasena().equals(repass)) {
                throw new RuntimeException("Las contraseñas deben ser iguales");
            }
            //TODO: Encriptar la contraseña - http://commons.apache.org/proper/commons-codec/download_codec.cgi
            usuario.setContrasena(DigestUtils.md5Hex(usuario.getContrasena()));
            return new UsuarioDAO().insertar(usuario);
        } else {
            return new UsuarioDAO().editar(usuario);
        }
    }

    public Usuario login(Usuario usuario) {
        if (usuario == null) {
            throw new RuntimeException("Debe crear un nuevo usuario");
        }

        if (usuario.getUsuario().isBlank()) {
            throw new RuntimeException("Usuario/Correo requerido");
        }

        if (usuario.getContrasena().length() < 8) {
            throw new RuntimeException("Contraseña con 8 caracteres como mínimo");
        }
        usuario.setContrasena(DigestUtils.md5Hex(usuario.getContrasena()));
        return new UsuarioDAO().auntenticar(usuario);
    }

    public LinkedList<Usuario> buscar(String filtro) {
        return new UsuarioDAO().seleccionar(filtro);
    }

    public void activar(Usuario u) {
        if (u.isActivo()) {
            return;
        }
        new UsuarioDAO().activarDesactivar(u, true);
    }

    public void desactivar(Usuario u) {
        if (!u.isActivo()) {
            return;
        }
        new UsuarioDAO().activarDesactivar(u, false);
    }

    public String generarPass() {

        char[] pass = new char[8];
        for (int i = 0; i < pass.length; i++) {
            int ran = (int) (Math.random() * 26) + 97;
            pass[i] = (char) ran;
        }

        for (int i = 0; i < 2; i++) {
            int ran = (int) (Math.random() * 10) + 48;
            int pos = (int) (Math.random() * 8);
            pass[pos] = (char) ran;
        }

        char[] sim = {'@', '$', '%', '*', '+'};

        while (true) {
            int pos = (int) (Math.random() * 8);
            if (Character.isLetter(pass[pos])) {
                int ran = (int) (Math.random() * sim.length);
                pass[pos] = sim[ran];
                break;
            }
        }

        while (true) {
            int pos = (int) (Math.random() * 8);
            if (Character.isLetter(pass[pos])) {
                pass[pos] = Character.toUpperCase(pass[pos]);
                break;
            }
        }

        return String.valueOf(pass);
    }

    public void restablecerPass(Usuario u) {
        u.setContrasena(DigestUtils.md5Hex(u.getContrasena()));
        new UsuarioDAO().editar(u);
    }

    public boolean asociar(Empleado e, Usuario u) {
        if (u == null) {
            throw new RuntimeException("Debe seleccinar un usuario");
        }
        if (e == null) {
            throw new RuntimeException("Debe seleccinar un empleado");
        }
        
        return new UsuarioDAO().asociar(e,u);
    }

}
