/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg08.rrhh.bo;

import java.time.LocalDate;
import java.time.Period;
import java.util.LinkedList;
import pkg08.rrhh.dao.EmpleadoDAO;
import pkg08.rrhh.entities.Empleado;
import pkg08.rrhh.entities.Solicitud;
import pkg08.rrhh.entities.Usuario;

/**
 *
 * @author amurilloa
 */
public class EmpleadoBO {

    public void activar(Empleado e) {
        if (e.isActivo()) {
            return;
        }

        e.setActivo(true);
        new EmpleadoDAO().editar(e);

    }

    public void desactivar(Empleado e) {
        if (!e.isActivo()) {
            return;
        }
        e.setActivo(false);
        new EmpleadoDAO().editar(e);
    }

    public LinkedList<Empleado> buscar(String filtro) {
        return new EmpleadoDAO().seleccionar(filtro);
    }

    public boolean registrar(Empleado empleado) {
        if (empleado == null) {
            throw new RuntimeException("Debe crear un nuevo empleado");
        }

        if (empleado.getNombre().isBlank()) {
            throw new RuntimeException("Nombre requerido");
        }
        if (empleado.getApellidoUno().isBlank()) {
            throw new RuntimeException("Primer Apellido requerido");
        }
        if (empleado.getCedula().isBlank()) {
            throw new RuntimeException("Cédula requerida");
        }
        if (empleado.getTelefono().isBlank()) {
            throw new RuntimeException("Teléfono requerido");
        }

        if (empleado.getId() <= 0) {
            return new EmpleadoDAO().insertar(empleado);
        } else {
            return new EmpleadoDAO().editar(empleado);
        }
    }

    public Empleado cargarEmp(Usuario u) {
        if (u == null) {
            throw new RuntimeException("Debe seleccionar un usuario");
        }

        return new EmpleadoDAO().cargarEmp(u.getId());
    }

    public LinkedList<Solicitud> cargarSolicitudes(Empleado empleado) {
        if (empleado == null) {
            throw new RuntimeException("Debe especificar el empleado");
        }
        return new EmpleadoDAO().cargarSolicitudes(empleado);
    }

    public boolean registrarSol(Solicitud s) {
        if (s == null) {
            throw new RuntimeException("Debe crear una solicitud");
        }
        if (s.getEstado() == 'S' && s.getEmpleado() == null) {
            throw new RuntimeException("Empleado es requerido");
        }
        if (s.getMotivo().isBlank()) {
            throw new RuntimeException("Motivo es requerido");
        }
        if (s.getEstado() == 'S' && s.getFechaInicio().isBefore(LocalDate.now())) {
            throw new RuntimeException("No puedo solicitar vacaciones para días pasados");
        }
        if (s.getFechaFin().isBefore(s.getFechaInicio())) {
            throw new RuntimeException("Fecha de Fin debe ser mayor que la Inicial");
        }

        if (s.getId() > 0) {
            return new EmpleadoDAO().editarSol(s);

        } else {
            return new EmpleadoDAO().insertarSol(s);
        }
    }

    public LinkedList<Solicitud> cargarSolicitudes() {
        return new EmpleadoDAO().cargarSolicitudes(null);
    }

    public int calcDias(Empleado empleado) {
        if (empleado.getId() > 0) {
            LocalDate contrato = empleado.getContrato().getFechaIngreso();
            LocalDate hoy = LocalDate.now();
            Period periodo = Period.between(contrato, hoy);
            int a = periodo.getYears();
            int m = periodo.getMonths();
            int dias = a * 12 + (a > 0 ? m : 0);

            for (Solicitud sol : cargarSolicitudes(empleado)) {
                if (sol.getEstado() == 'A') {
                    dias -= sol.getDias();
                }
            }

            return dias;
        }
        return 0;
    }

}
