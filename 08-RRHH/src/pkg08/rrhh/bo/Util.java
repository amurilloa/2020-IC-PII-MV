/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg08.rrhh.bo;

import java.awt.Image;
import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.ImageIcon;

/**
 *
 * @author amurilloa
 */
public class Util {

    public static ImageIcon resize(int ancho, int alto, ImageIcon image) {
        Image newimage = image.getImage().getScaledInstance(ancho, alto, Image.SCALE_SMOOTH);
        return new ImageIcon(newimage);
    }



    public static boolean sendMail(String destinatario, String asunto, String cuerpo) {
        final String USER = "allan.utn.test@gmail.com";
        final String PASS = "Qw22erty";

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(USER, PASS);
            }
        });

        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(USER));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(destinatario));
            message.setSubject(asunto);
//            message.setText(cuerpo);
            message.setContent(cuerpo, "text/html");
            Transport.send(message);
            return true;
        } catch (MessagingException e) {
            System.out.println(e.getMessage());
            throw new RuntimeException("No se pudo enviar el correo");
        }

    }
}
