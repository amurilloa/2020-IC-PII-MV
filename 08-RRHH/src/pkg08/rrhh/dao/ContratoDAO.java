/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg08.rrhh.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import pkg08.rrhh.entities.Contrato;
import pkg08.rrhh.entities.Empleado;
import pkg08.rrhh.entities.Puesto;

/**
 *
 * @author amurilloa
 */
public class ContratoDAO {

    
    
    
    public boolean insertar(Contrato c, Empleado emp) {
         try ( Connection con = Conexion.getConexion()) {
            String sql = "insert into rrhh.contratos(id_empleado, id_puesto, "
                    + " fecha, salario, activo) values (?,?,?,?,?)";
            PreparedStatement stm = con.prepareStatement(sql);
            stm.setInt(1, emp.getId());
            stm.setInt(2, c.getPuesto().getId());
            stm.setDate(3, Date.valueOf(c.getFechaIngreso()));
            stm.setDouble(4, c.getSalario());
            stm.setBoolean(5, c.isActivo());
            return stm.executeUpdate() == 1;

        } catch (Exception ex) {
            String msj = "Problemas al registrar el Contrato";
            throw new RuntimeException(msj);
        }
    }

    public Contrato cargarEmp(int idEmpleado) {
       try ( Connection con = Conexion.getConexion()) {
            String sql = "select  id, id_puesto, fecha, salario, activo from rrhh.contratos "
                    + " where id_empleado = ? order by fecha desc limit 1";
            PreparedStatement stm = con.prepareStatement(sql);
            stm.setInt(1, idEmpleado);

            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                return cargar(rs);
            }

        } catch (Exception ex) {
            throw new RuntimeException("Favor intente nuevamente");
        }
        return null;
    }

    private Contrato cargar(ResultSet rs) throws SQLException {
        Contrato c = new Contrato();
        c.setId(rs.getInt(1));
        Puesto p = new Puesto();
        p.setId(rs.getInt(2));
        c.setPuesto(p);
        c.setFechaIngreso(rs.getDate(3).toLocalDate());
        c.setSalario(rs.getDouble(4));
        c.setActivo(rs.getBoolean(5));
        return c;
    }
    
}
