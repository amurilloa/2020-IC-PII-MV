/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg08.rrhh.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import pkg08.rrhh.entities.Area;
import pkg08.rrhh.entities.Puesto;

/**
 *
 * @author amurilloa
 */
public class AreaDAO {

    public LinkedList<Area> cargarA(boolean activas) {
        LinkedList<Area> areas = new LinkedList<>();

        try ( Connection con = Conexion.getConexion()) {
            String sql = "SELECT id, codigo, nombre, activo FROM rrhh.areas";

            if (activas) {
                sql += " where activo = true";
            }

            PreparedStatement stm = con.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                areas.add(cargarA(rs));
            }

        } catch (Exception ex) {
            throw new RuntimeException("Favor intente nuevamente");
        }

        return areas;
    }

    private Area cargarA(ResultSet rs) throws SQLException {
        Area a = new Area();
        a.setId(rs.getInt(1));
        a.setCodigo(rs.getString(2));
        a.setNombre(rs.getString(3));
        a.setActivo(rs.getBoolean(4));
        return a;

    }

    public LinkedList<Puesto> cargarP(Area a, boolean activos) {
         LinkedList<Puesto> puestos = new LinkedList<>();
         
         try ( Connection con = Conexion.getConexion()) {
            String sql = "SELECT id,  codigo, nombre, salario, activo FROM rrhh.puestos where id_area = ?";
          
            if(activos){
                sql += " and activo = true";
            }
            
            PreparedStatement stm = con.prepareStatement(sql);
            stm.setInt(1, a.getId());
            
            
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                puestos.add(cargarP(rs));
            }

        } catch (Exception ex) {
            
            throw new RuntimeException("Favor intente nuevamente");
        }

        return puestos;
    }

        private Puesto cargarP(ResultSet rs) throws SQLException {
        Puesto p = new Puesto();
        p.setId(rs.getInt(1));
        p.setCodigo(rs.getString(2));
        p.setNombre(rs.getString(3));
        p.setSalario(rs.getDouble(4));
        p.setActivo(rs.getBoolean(5));
        return p;

    }
}
