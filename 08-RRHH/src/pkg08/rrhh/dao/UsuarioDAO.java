/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg08.rrhh.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import pkg08.rrhh.entities.Empleado;
import pkg08.rrhh.entities.Usuario;

/**
 *
 * @author amurilloa
 */
public class UsuarioDAO {

    public boolean insertar(Usuario usuario) {
        try ( Connection con = Conexion.getConexion()) {
            String sql = "INSERT INTO  rrhh.usuarios(usuario, correo, contrasena) VALUES (?, ?, ?) returning id";
            PreparedStatement stm = con.prepareStatement(sql);
            stm.setString(1, usuario.getUsuario());
            stm.setString(2, usuario.getCorreo());
            stm.setString(3, usuario.getContrasena());
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                int id = rs.getInt(1);
                usuario.setId(id);
                new FotoDAO().insertar(usuario);
                //Insertar la foto(Foto, idUsuario)
                return true;
            }

        } catch (Exception ex) {
            String msj = ex.getMessage().contains("unq_usuarios_usuario")
                    ? "Usuario previamente registrado"
                    : ex.getMessage().contains("usuarios_correo_key")
                    ? "Correo previamente registrado"
                    : "Problemas al registrar el Usuario";
            throw new RuntimeException(msj);
        }
        return false;
    }

    public Usuario auntenticar(Usuario usuario) {
        try ( Connection con = Conexion.getConexion()) {
            String sql = "select u.id,u.usuario, u.correo, u.contrasena, u.activo, (a.id=1) as admin from rrhh.usuarios u "
                    + " inner join rrhh.contratos c on u.id_empleado = c.id_empleado "
                    + " inner join rrhh.puestos p on c.id_puesto = p.id "
                    + " inner join rrhh.areas a on p.id_area = a.id "
                    + " where u.activo and u.contrasena = ? and (usuario = ? or correo = ?) "
                    + " order by c.fecha desc limit 1";
            PreparedStatement stm = con.prepareStatement(sql);
            stm.setString(1, usuario.getContrasena());
            stm.setString(2, usuario.getUsuario());
            stm.setString(3, usuario.getUsuario());

            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                return cargar(rs);
            }

        } catch (Exception ex) {
            throw new RuntimeException("Favor intente nuevamente");
        }
        return null;
    }

    private Usuario cargar(ResultSet rs) throws SQLException {
        Usuario u = new Usuario();
        u.setId(rs.getInt(1));
        u.setUsuario(rs.getString(2));
        u.setCorreo(rs.getString(3));
        u.setContrasena(rs.getString(4));
        u.setActivo(rs.getBoolean(5));
        u.setFoto(new FotoDAO().cargar(u.getId()));
        if (rs.getMetaData().getColumnCount() > 5) {
            u.setAdmin(rs.getBoolean(6));
        }
        return u;
    }

    public LinkedList<Usuario> seleccionar(String filtro) {
        LinkedList<Usuario> usuarios = new LinkedList<>();

        try ( Connection con = Conexion.getConexion()) {
            String sql = "select id,usuario, correo, contrasena, activo from rrhh.usuarios "
                    + " where lower(usuario) like lower(?) or lower(correo) like lower(?) "
                    + " order by usuario";
            PreparedStatement stm = con.prepareStatement(sql);
            stm.setString(1, filtro + '%');
            stm.setString(2, filtro + '%');

            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                usuarios.add(cargar(rs));
            }

        } catch (Exception ex) {
            throw new RuntimeException("Favor intente nuevamente");
        }
        return usuarios;
    }

    public void activarDesactivar(Usuario u, boolean estado) {
        try ( Connection con = Conexion.getConexion()) {
            String sql = "update rrhh.usuarios set activo = ? where id = ? ";
            PreparedStatement stm = con.prepareStatement(sql);
            stm.setBoolean(1, estado);
            stm.setInt(2, u.getId());

            stm.executeUpdate();

        } catch (Exception ex) {
            throw new RuntimeException("Favor intente nuevamente");
        }
    }

    public boolean editar(Usuario usuario) {
        try ( Connection con = Conexion.getConexion()) {
            String sql = "update rrhh.usuarios set usuario = ?, correo = ?, contrasena = ?, activo = ? where id = ?";
            PreparedStatement stm = con.prepareStatement(sql);
            stm.setString(1, usuario.getUsuario());
            stm.setString(2, usuario.getCorreo());
            stm.setString(3, usuario.getContrasena());
            stm.setBoolean(4, usuario.isActivo());
            stm.setInt(5, usuario.getId());
            return stm.executeUpdate() == 1;

        } catch (Exception ex) {
            throw new RuntimeException("Favor intente nuevamente");
        }
    }

    public boolean asociar(Empleado e, Usuario u) {
        try ( Connection con = Conexion.getConexion()) {
            String sql = "update rrhh.usuarios set id_empleado = null where id_empleado = ?";
            PreparedStatement stm = con.prepareStatement(sql);
            stm.setInt(1, e.getId());
            stm.execute();
            sql = "update rrhh.usuarios set id_empleado = ? where id = ?";
            stm = con.prepareStatement(sql);
            stm.setInt(1, e.getId());
            stm.setInt(2, u.getId());
            return stm.executeUpdate() == 1;
        } catch (Exception ex) {
            throw new RuntimeException("Favor intente nuevamente");
        }
    }

    public Usuario cargarUsu(int idEmpleado) {
        try ( Connection con = Conexion.getConexion()) {
            String sql = "select id, usuario, correo, contrasena, activo from rrhh.usuarios "
                    + " where id_empleado = ?";
            PreparedStatement stm = con.prepareStatement(sql);
            stm.setInt(1, idEmpleado);

            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                return cargar(rs);
            }

        } catch (Exception ex) {
            throw new RuntimeException("Favor intente nuevamente");
        }
        return null;
    }

}
