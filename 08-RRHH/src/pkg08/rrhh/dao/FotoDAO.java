/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg08.rrhh.dao;

import java.awt.Graphics;
import java.awt.Image;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import pkg08.rrhh.entities.Usuario;
import java.io.ByteArrayOutputStream;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.Buffer;
import java.sql.SQLException;
import javax.imageio.ImageIO;
import pkg08.rrhh.entities.Foto;

/**
 *
 * @author amurilloa
 */
public class FotoDAO {

    public void insertar(Usuario usuario) {
        try ( Connection con = Conexion.getConexion()) {
            String sql = "INSERT INTO  rrhh.imagenes(id_usuario, imagen) VALUES ( ?, ?)";
            PreparedStatement stm = con.prepareStatement(sql);
            stm.setInt(1, usuario.getId());

            BufferedImage bf = imageToBufferedImage(usuario.getFoto().getFoto());

            ByteArrayOutputStream os = new ByteArrayOutputStream();
            ImageIO.write(bf, "jpg", os);
            InputStream in = new ByteArrayInputStream(os.toByteArray());

            stm.setBinaryStream(2, in);
            stm.execute();
        } catch (Exception ex) {
            ex.printStackTrace();
            String msj = "Problemas al registrar el Usuario";
            throw new RuntimeException(msj);
        }
    }

    public static BufferedImage imageToBufferedImage(Image im) {
        BufferedImage bi = new BufferedImage(im.getWidth(null), im.getHeight(null), BufferedImage.TYPE_INT_RGB);
        Graphics bg = bi.getGraphics();
        bg.drawImage(im, 0, 0, null);
        bg.dispose();
        return bi;
    }

    public Foto cargar(int idUsuario) {
        try ( Connection con = Conexion.getConexion()) {
            String sql = "select id, imagen from rrhh.imagenes where id_usuario = ?";
            PreparedStatement stm = con.prepareStatement(sql);
            stm.setInt(1, idUsuario);

            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                return cargar(rs);
            }
            return null;
        } catch (Exception ex) {
            ex.printStackTrace();
            String msj = "Problemas al registrar el Usuario";
            throw new RuntimeException(msj);
        }
    }

    private Foto cargar(ResultSet rs) throws SQLException, IOException {
        Foto f = new Foto();
        f.setId(rs.getInt(1));
        Image imgdb = null;
        InputStream fis = rs.getBinaryStream(2);
        imgdb = ImageIO.read(fis);
        f.setFoto(imgdb);
        return f;
    }

}
