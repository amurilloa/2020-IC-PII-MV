/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg08.rrhh.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Date;
import java.util.LinkedList;
import javax.lang.model.util.Types;
import pkg08.rrhh.entities.Empleado;
import pkg08.rrhh.entities.Solicitud;

/**
 *
 * @author amurilloa
 */
public class EmpleadoDAO {

    public LinkedList<Empleado> seleccionar(String filtro) {
        LinkedList<Empleado> empleados = new LinkedList<>();

        try ( Connection con = Conexion.getConexion()) {
            String sql = "select id,cedula, nombre, apellido_uno, apellido_dos, telefono, genero, estado_civil, direccion, activo from rrhh.empleados "
                    + " where lower(cedula) like lower(?) or lower(apellido_uno) like lower(?) "
                    + " order by apellido_uno";
            PreparedStatement stm = con.prepareStatement(sql);
            stm.setString(1, filtro + '%');
            stm.setString(2, filtro + '%');

            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                empleados.add(cargar(rs));
            }

        } catch (Exception ex) {
            throw new RuntimeException("Favor intente nuevamente");
        }
        return empleados;
    }

    private Empleado cargar(ResultSet rs) throws SQLException {
        Empleado e = new Empleado();
        e.setId(rs.getInt(1));
        e.setCedula(rs.getString(2));
        e.setNombre(rs.getString(3));
        e.setApellidoUno(rs.getString(4));
        e.setApellidoDos(rs.getString(5));
        e.setTelefono(rs.getString(6));
        e.setGenero(rs.getString(7).charAt(0));
        e.setEstadoCivil(rs.getString(8).charAt(0));
        e.setDireccion(rs.getString(9));
        e.setActivo(rs.getBoolean(10));
        e.setUsuario(new UsuarioDAO().cargarUsu(e.getId()));
        e.setContrato(new ContratoDAO().cargarEmp(e.getId()));
        return e;
    }

    public boolean insertar(Empleado e) {
        try ( Connection con = Conexion.getConexion()) {
            String sql = "insert into rrhh.empleados(cedula, nombre, apellido_uno, "
                    + " apellido_dos, telefono, genero, estado_civil, direccion, activo) "
                    + " values (?, ?, ?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement stm = con.prepareStatement(sql);
            stm.setString(1, e.getCedula());
            stm.setString(2, e.getNombre());
            stm.setString(3, e.getApellidoUno());
            stm.setString(4, e.getApellidoDos());
            stm.setString(5, e.getTelefono());
            stm.setString(6, String.valueOf(e.getGenero()));
            stm.setString(7, String.valueOf(e.getEstadoCivil()));
            stm.setString(8, e.getDireccion());
            stm.setBoolean(9, e.isActivo());

            return stm.executeUpdate() == 1;

        } catch (Exception ex) {
            String msj = ex.getMessage().contains("unq_empleados_cedula")
                    ? "Cédula previamente registrado"
                    : "Problemas al registrar el Empleado";
            throw new RuntimeException(msj);
        }
    }

    public boolean editar(Empleado e) {
        try ( Connection con = Conexion.getConexion()) {
            String sql = "update rrhh.empleados set cedula = ?, nombre = ?, "
                    + " apellido_uno = ?,  apellido_dos = ?, telefono = ?, "
                    + " genero  = ?, estado_civil = ?, direccion = ?, activo  = ? "
                    + " where id = ?";
            PreparedStatement stm = con.prepareStatement(sql);
            stm.setString(1, e.getCedula());
            stm.setString(2, e.getNombre());
            stm.setString(3, e.getApellidoUno());
            stm.setString(4, e.getApellidoDos());
            stm.setString(5, e.getTelefono());
            stm.setString(6, String.valueOf(e.getGenero()));
            stm.setString(7, String.valueOf(e.getEstadoCivil()));
            stm.setString(8, e.getDireccion());
            stm.setBoolean(9, e.isActivo());
            stm.setInt(10, e.getId());
            return stm.executeUpdate() == 1;

        } catch (Exception ex) {
            throw new RuntimeException("Favor intente nuevamente");
        }
    }

    public Empleado cargarEmp(int idUsuario) {
        try ( Connection con = Conexion.getConexion()) {
            String sql = "select e.id,e.cedula, e.nombre, e.apellido_uno, e.apellido_dos, e.telefono, "
                    + " e.genero, e.estado_civil, e.direccion, e.activo "
                    + " from rrhh.empleados e "
                    + " inner join rrhh.usuarios u on e.id = u.id_empleado "
                    + " where u.id = ?";
            PreparedStatement stm = con.prepareStatement(sql);
            stm.setInt(1, idUsuario);

            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                return cargar(rs);
            }

        } catch (Exception ex) {
            throw new RuntimeException("Favor intente nuevamente");
        }
        return null;
    }

    public Empleado cargarID(int id) {
        try ( Connection con = Conexion.getConexion()) {
            String sql = "select e.id,e.cedula, e.nombre, e.apellido_uno, e.apellido_dos, e.telefono, "
                    + " e.genero, e.estado_civil, e.direccion, e.activo "
                    + " from rrhh.empleados e "
                    + " where e.id = ?";
            PreparedStatement stm = con.prepareStatement(sql);
            stm.setInt(1, id);

            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                return cargar(rs);
            }

        } catch (Exception ex) {
            throw new RuntimeException("Favor intente nuevamente");
        }
        return null;
    }

    public LinkedList<Solicitud> cargarSolicitudes(Empleado empleado) {
        LinkedList<Solicitud> solicitudes = new LinkedList<>();

        try ( Connection con = Conexion.getConexion()) {
            String sql = "select id, id_empleado, motivo, fecha_ini, fecha_fin, "
                    + " dias, observacion, estado from rrhh.vacaciones where ";

            if (empleado != null) {
                sql += "  id_empleado = ? or id_empleado is null order by fecha_ini";
            } else {
                sql += "  estado = 'S' order by fecha_ini";
            }

            PreparedStatement stm = con.prepareStatement(sql);
            if (empleado != null) {
                stm.setInt(1, empleado.getId());
            }

            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                solicitudes.add(cargarSol(rs));
            }

        } catch (Exception ex) {
            throw new RuntimeException("Favor intente nuevamente");
        }
        return solicitudes;
    }

    private Solicitud cargarSol(ResultSet rs) throws SQLException {
        Solicitud s = new Solicitud();
        s.setId(rs.getInt(1));
        if (rs.getObject(2) == null) {
            Empleado e = new Empleado();
            e.setNombre("Global");
            e.setApellidoUno("");
            e.setApellidoDos("");
            s.setEmpleado(e);
        } else {
            s.setEmpleado(cargarID(rs.getInt(2)));
        }
        s.setMotivo(rs.getString(3));
        s.setFechaInicio(rs.getDate(4).toLocalDate());
        s.setFechaFin(rs.getDate(5).toLocalDate());
        s.setDias(rs.getInt(6));
        s.setObservacion(rs.getString(7));
        s.setEstado(rs.getString(8).charAt(0));
        return s;
    }

    public boolean insertarSol(Solicitud s) {
        try ( Connection con = Conexion.getConexion()) {
            String sql = "insert into rrhh.vacaciones(id_empleado, motivo, fecha_ini, fecha_fin, estado, dias, observacion) "
                    + " values(?,?,?,?,?,?,?)";
            PreparedStatement stm = con.prepareStatement(sql);
            if (s.getEmpleado() == null) {
                stm.setNull(1, java.sql.Types.INTEGER);
            } else {
                stm.setInt(1, s.getEmpleado().getId());
            }
            stm.setString(2, s.getMotivo());
            stm.setDate(3, Date.valueOf(s.getFechaInicio()));
            stm.setDate(4, Date.valueOf(s.getFechaFin()));
            stm.setString(5, String.valueOf(s.getEstado()));
            stm.setInt(6, s.getDias());
            stm.setString(7, s.getObservacion());

            return stm.executeUpdate() == 1;

        } catch (Exception ex) {
            String msj = "Problemas al registrar el Empleado";
            throw new RuntimeException(msj);
        }
    }

    public boolean editarSol(Solicitud s) {
        try ( Connection con = Conexion.getConexion()) {
            String sql = "update rrhh.vacaciones set  motivo=?, fecha_ini=?, "
                    + " fecha_fin=?, dias=?, observacion=?, estado=? where id =?";
            PreparedStatement stm = con.prepareStatement(sql);
            stm.setString(1, s.getMotivo());
            stm.setDate(2, Date.valueOf(s.getFechaInicio()));
            stm.setDate(3, Date.valueOf(s.getFechaFin()));
            stm.setInt(4, s.getDias());
            stm.setString(5, s.getObservacion());
            stm.setString(6, String.valueOf(s.getEstado()));
            stm.setInt(7, s.getId());
            return stm.executeUpdate() == 1;

        } catch (Exception ex) {
            String msj = "Problemas al registrar el Empleado";
            throw new RuntimeException(msj);
        }
    }
}
