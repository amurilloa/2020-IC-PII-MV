/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg08.rrhh.entities;

/**
 *
 * @author amurilloa
 */
public class Puesto {
    private int id;
//    private Area area;
    private String codigo;
    private String nombre;
    private double salario;
    private boolean activo;

    public Puesto() {
    }

    public Puesto(int id, String codigo, String nombre, double salario, boolean activo) {
        this.id = id;
        this.codigo = codigo;
        this.nombre = nombre;
        this.salario = salario;
        this.activo = activo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    @Override
    public String toString() {
        return codigo + " - " + nombre;
    }
}
