/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg08.rrhh.entities;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author amurilloa
 */
public class Solicitud {

    private int id;
    private Empleado empleado;
    private String motivo;
    private LocalDate fechaInicio;
    private LocalDate fechaFin;
    private int dias;
    private String observacion;
    private char estado;

    public Solicitud() {
        fechaInicio = LocalDate.now();
        fechaFin = LocalDate.now();
        estado = 'S';
    }

    public Solicitud(int id, Empleado empleado, String motivo, LocalDate fechaInicio, LocalDate fechaFin, int dias, String observacion, char estado) {
        this.id = id;
        this.empleado = empleado;
        this.motivo = motivo;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.dias = dias;
        this.observacion = observacion;
        this.estado = estado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Empleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public LocalDate getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(LocalDate fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public LocalDate getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(LocalDate fechaFin) {
        this.fechaFin = fechaFin;
    }

    public int getDias() {
        return dias;
    }

    public void setDias(int dias) {
        this.dias = dias;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public char getEstado() {
        return estado;
    }

    public void setEstado(char estado) {
        this.estado = estado;
    }

    public String getEstadoTxT() {
        switch (estado) {
            case 'A':
                return "Aprobada";
            case 'R':
                return "Rechazada";
            default:
                return "Solicitada";
        }
    }

    @Override
    public String toString() {
        String fi = fechaInicio.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        String ff = fechaFin.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        String es = estado == 'A' ? " ✓" : estado == 'R' ? " ✘" : " ✎";
        return motivo + " - " + fi + " al " + ff + es;
    }
}
