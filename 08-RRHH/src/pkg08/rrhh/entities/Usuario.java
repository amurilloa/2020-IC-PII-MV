/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg08.rrhh.entities;

/**
 *
 * @author amurilloa
 */
public class Usuario {

    private int id;
    private String usuario;
    private String contrasena;
    private String correo;
    private boolean activo;
    private Foto foto;
    private boolean admin;

    public Usuario() {
    }

    public Usuario(int id, String usuario, String contrasena, String correo, boolean activo) {
        this.id = id;
        this.usuario = usuario;
        this.contrasena = contrasena;
        this.correo = correo;
        this.activo = activo;
    }

    public Usuario(int id, String usuario, String contrasena, String correo, boolean activo, Foto foto) {
        this.id = id;
        this.usuario = usuario;
        this.contrasena = contrasena;
        this.correo = correo;
        this.activo = activo;
        this.foto = foto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public Foto getFoto() {
        return foto;
    }

    public void setFoto(Foto foto) {
        this.foto = foto;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    
    @Override
    public String toString() {
        return String.format("%s | %s | %s", usuario, correo, (activo ? "	✓":"✘"));
    }
}
