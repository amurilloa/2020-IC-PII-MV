/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg08.rrhh.entities;

import java.time.LocalDate;

/**
 *
 * @author amurilloa
 */
public class Contrato {
    
    private int id;
//    private Empleado empleado;
    private Puesto puesto;
    private LocalDate fechaIngreso; 
    private double salario; 
    private boolean activo;

    public Contrato() {
    }

    public Contrato(int id, Puesto puesto, LocalDate fechaIngreso, double salario, boolean activo) {
        this.id = id;
        this.puesto = puesto;
        this.fechaIngreso = fechaIngreso;
        this.salario = salario;
        this.activo = activo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Puesto getPuesto() {
        return puesto;
    }

    public void setPuesto(Puesto puesto) {
        this.puesto = puesto;
    }

    public LocalDate getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(LocalDate fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    @Override
    public String toString() {
        return "Contrato{" + "id=" + id + ", puesto=" + puesto + ", fechaIngreso=" + fechaIngreso + ", salario=" + salario + ", activo=" + activo + '}';
    }
   
}
