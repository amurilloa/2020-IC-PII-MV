/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg08.rrhh.entities;

/**
 *
 * @author amurilloa
 */
public class Empleado {

    private int id;
    private String cedula;
    private String nombre;
    private String apellidoUno;
    private String apellidoDos;
    private String telefono;
    private char genero;
    private char estadoCivil;
    private String direccion;
    private boolean activo;
    private Contrato contrato;
    private Usuario usuario;
    

    public Empleado() {
    }

    public Empleado(int id, String cedula, String nombre, String apellidoUno, String apellidoDos, String telefono, char genero, char estadoCivil, String direccion, boolean activo) {
        this.id = id;
        this.cedula = cedula;
        this.nombre = nombre;
        this.apellidoUno = apellidoUno;
        this.apellidoDos = apellidoDos;
        this.telefono = telefono;
        this.genero = genero;
        this.estadoCivil = estadoCivil;
        this.direccion = direccion;
        this.activo = activo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoUno() {
        return apellidoUno;
    }

    public void setApellidoUno(String apellidoUno) {
        this.apellidoUno = apellidoUno;
    }

    public String getApellidoDos() {
        return apellidoDos;
    }

    public void setApellidoDos(String apellidoDos) {
        this.apellidoDos = apellidoDos;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public char getGenero() {
        return genero;
    }

    public String getGeneroTxT() {
        return genero == 'M' ? "Masc." : "Femen.";
    }

    public void setGenero(char genero) {
        this.genero = genero;
    }

    public char getEstadoCivil() {
        return estadoCivil;
    }

    public String getEstadoCivilTxT() {
        switch (estadoCivil) {
            case 'D':
                return "Divorciado/a";
            case 'V':
                return "Viudo/a";
            case 'C':
                return "Casado/a";
            case 'U':
                return "Unión Libre";
            default:
                return "Soltero/a";
        }
    }

    public void setEstadoCivil(char estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public String getNombreComp() {
        return String.format("%s %s %s", nombre, apellidoUno, apellidoDos);
    }

    public Contrato getContrato() {
        return contrato;
    }

    public void setContrato(Contrato contrato) {
        this.contrato = contrato;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    @Override
    public String toString() {
        return "Empleado{" + "id=" + id + ", cedula=" + cedula + ", nombre=" + nombre + ", apellidoUno=" + apellidoUno + ", apellidoDos=" + apellidoDos + ", telefono=" + telefono + ", genero=" + genero + ", estadoCivil=" + estadoCivil + ", direccion=" + direccion + ", activo=" + activo + '}';
    }

}
