/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg05.tiendamusical.app;

import java.util.LinkedList;
import pkg05.tiendamusical.entidades.DiscoCompacto;
import pkg05.tiendamusical.entidades.DiscoVersatilDigital;
import pkg05.tiendamusical.entidades.DiscoVinilo;
import pkg05.tiendamusical.entidades.ProductoMusical;

/**
 *
 * @author ALLAN
 */
public class Logica {

    private final LinkedList<ProductoMusical> productos;

    public Logica() {
        productos = new LinkedList<>();
        datosPrueba();
    }

    public boolean registrar(ProductoMusical pro) {
        //Validar que el código no exista
        for (ProductoMusical p : productos) {
            if (p != null && p.getCodigo().equals(pro.getCodigo())) {
                return false;
            }
        }
        //si no existe que lo registre
        productos.add(pro);
        return true;
    }

    public LinkedList<ProductoMusical> getProductos() {
        return productos;
    }

    private void datosPrueba() {
        registrar(new DiscoCompacto("C0001", "RollingStones Records", "Metallica", "12cm", "mp3", "CD-ROM"));
        registrar(new DiscoCompacto("C0002", "Universal r. Music Group Distribution", "Aerosmith", "12cm", "mp3", "CD-ROM"));
        registrar(new DiscoCompacto("C0003", "Cash Money Records", "Pink", "12cm", "mp3", "CD-ROM"));
        registrar(new DiscoVersatilDigital("D0001", "Toei Animation", "Akira Toriyama", "mkv", "DVD-RW"));
        registrar(new DiscoVinilo("V0001", "Sony Music Entertainment", "Santana", "24cm", 33));
    }

    public boolean eliminar(ProductoMusical pro) {
        return productos.remove(pro);
    }

    public String imprimir(ProductoMusical pro) {
        /*
        ------ Vinilo ------
        Código: V0001
         Sello: Sony Music
         Autor: Santana
        Tamaño: 24cm
           RPM: 33
         */
        String txt = "Tipo seleccionado: %s\n"
                + "Código: %s\n"
                + "Sello: %s\n"
                + "Autor: %s\n";
        if (pro instanceof DiscoCompacto) {
            DiscoCompacto cd = (DiscoCompacto) pro;
            txt += "Tamaño: %s\n"
                    + "Formato: %s\n"
                    + "Tipo: %s";
            txt = String.format(txt, "CD", cd.getCodigo(), cd.getSello(), cd.getAutor(), cd.getTamano(),
                    cd.getFormato(), cd.getTipo());
        } else if (pro instanceof DiscoVersatilDigital) {
            DiscoVersatilDigital dvd = (DiscoVersatilDigital) pro;
            txt += "Formato: %s\n"
                    + "Tipo: %s";
            txt = String.format(txt, "DVD", dvd.getCodigo(), dvd.getSello(), dvd.getAutor(),
                    dvd.getFormato(), dvd.getTipo());
        } else if (pro instanceof DiscoVinilo) {
            DiscoVinilo dv = (DiscoVinilo) pro;
            txt += "Tamaño: %s\n"
                    + "RPM: %d";
            txt = String.format(txt, "Disco de Vinilo", dv.getCodigo(), dv.getSello(), dv.getAutor(),
                    dv.getTamano(), dv.getRpm());
        }
        return txt;
    }

    public boolean editar(ProductoMusical nuevo, ProductoMusical viejo) {
        for (int i = 0; i < productos.size(); i++) {
            if (productos.get(i) != null && productos.get(i).getCodigo().equals(viejo.getCodigo())) {
                productos.remove(i);
                productos.add(i, nuevo);
                return true;
            }
        }

        return false;
    }

}
