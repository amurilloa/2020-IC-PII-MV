/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg05.tiendamusical.app;

import java.awt.Container;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;

/**
 *
 * @author ALLAN
 */
public class Util {

    public static void limpiar(Container con) {
        for (Object component : con.getComponents()) {
            if (component instanceof JTextField) {
                ((JTextField) component).setText("");
            } else if (component instanceof JSpinner) {
                ((JSpinner) component).setValue(33);
            } else if (component instanceof JTabbedPane || component instanceof JPanel) {
                limpiar((Container) component);
            }
        }
    }

}
