/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg05.tiendamusical.entidades;

/**
 *
 * @author ALLAN
 */
public class DiscoVinilo extends ProductoMusical {

    private String tamano;
    private int rpm;

    public DiscoVinilo() {
    }

    public DiscoVinilo(String codigo, String sello, String autor, String tamano, int rpm) {
        super(codigo, sello, autor);
        this.tamano = tamano;
        this.rpm = rpm;
    }

    public String getTamano() {
        return tamano;
    }

    public void setTamano(String tamano) {
        this.tamano = tamano;
    }

    public int getRpm() {
        return rpm;
    }

    public void setRpm(int rpm) {
        this.rpm = rpm;
    }

    @Override
    public String toString() {
        String txt = String.format("%s", codigo) + " - " + autor + " (" + sello + ")";
        return txt;
    }

}
