/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg05.tiendamusical.entidades;

/**
 *
 * @author ALLAN
 */
public abstract class ProductoMusical {
    
    protected String codigo;
    protected String sello;
    protected String autor;

    public ProductoMusical() {
    }

    public ProductoMusical(String codigo, String sello, String autor) {
        this.codigo = codigo;
        this.sello = sello;
        this.autor = autor;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getSello() {
        return sello;
    }

    public void setSello(String sello) {
        this.sello = sello;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    @Override
    public String toString() {
        return "ProductoMusical{" + "codigo=" + codigo + ", sello=" + sello + ", autor=" + autor + '}';
    }
    
}
