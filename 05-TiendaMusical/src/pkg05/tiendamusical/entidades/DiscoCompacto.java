/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg05.tiendamusical.entidades;

/**
 *
 * @author ALLAN
 */
public class DiscoCompacto extends ProductoMusical {

    private String tamano;
    private String formato;
    private String tipo;

    public DiscoCompacto() {
    }

    public DiscoCompacto(String codigo, String sello, String autor, String tamano, String formato, String tipo) {
        super(codigo, sello, autor);
        this.tamano = tamano;
        this.formato = formato;
        this.tipo = tipo;
    }

    public String getTamano() {
        return tamano;
    }

    public void setTamano(String tamano) {
        this.tamano = tamano;
    }

    public String getFormato() {
        return formato;
    }

    public void setFormato(String formato) {
        this.formato = formato;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        String txt = String.format("%s", codigo) + " - " + autor + " (" + sello + ")";
        return txt;
    }

}
