-- create database progrados;
create schema bs;
-- drop table bs.areas
create table bs.areas(
	id serial primary key, 
	codigo text not null unique,
	nombre text not null, 
	activo boolean default true
);
x
-- drop table bs.puestos
create table bs.puestos(
	id serial primary key,
	id_area integer not null,
	codigo text not null,
	nombre text not null, 
	salario_base numeric default 0,
	activo boolean default true, 
	constraint  fk_puestos_areas foreign key (id_area) references bs.areas(id), 
	constraint unq_area_cod unique(id_area, codigo)
);

drop table bs.usuarios

CREATE TABLE bs.usuarios
(
    id serial primary key,
    usuario text NOT NULL,
	correo text NOT NULL UNIQUE, 
    contrasena text NOT NULL,
    activo boolean DEFAULT true,
    CONSTRAINT unq_usuarios_usuario UNIQUE (usuario)
);

--alter table bs.usuarios add column correo text not null default ' ' unique 

-- insert into bs.areas(codigo, nombre) values ('CONT', 'Contabilidad')
-- insert into bs.areas(codigo, nombre) values ('FINA', 'Financiero')
-- select * from bs.areas
select * from bs.puestos
insert into bs.puestos(id_area, codigo, nombre, salario_base) 
values (3, 'GTE', 'Gerente', 650000 )

-- delete from bs.areas where id = 17
-- update bs.puestos set salario_base = 675000 where id=3

select * from bs.usuarios
