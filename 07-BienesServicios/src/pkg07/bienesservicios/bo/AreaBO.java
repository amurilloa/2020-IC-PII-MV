/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg07.bienesservicios.bo;

import java.util.LinkedList;
import pkg07.bienesservicios.dao.AreaDAO;
import pkg07.bienesservicios.entities.Area;

/**
 *
 * @author amurilloa
 */
public class AreaBO {

    public void registrar(Area area) {
        if (area == null) {
            throw new RuntimeException("Debe crear un área primero");
        }

        if (area.getCodigo().isBlank()) {
            throw new RuntimeException("Código es requerido");
        }
        
        if (area.getNombre().isBlank()) {
            throw new RuntimeException("Nombre es requerido");
        }
        
        new AreaDAO().insertar(area);
    }

    public LinkedList<Area> cargar() {     
        return new AreaDAO().cargar();
    }

}
