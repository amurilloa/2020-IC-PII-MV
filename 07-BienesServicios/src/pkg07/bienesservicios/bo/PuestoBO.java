/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg07.bienesservicios.bo;

import java.util.LinkedList;
import pkg07.bienesservicios.dao.PuestoDAO;
import pkg07.bienesservicios.entities.Area;
import pkg07.bienesservicios.entities.Puesto;

/**
 *
 * @author amurilloa
 */
public class PuestoBO {
    
    public void registrar(Puesto puesto) {
        if (puesto == null) {
            throw new RuntimeException("Debe crear un puesto primero");
        }

        if (puesto.getCodigo().isBlank()) {
            throw new RuntimeException("Código es requerido");
        }
        
        if (puesto.getNombre().isBlank()) {
            throw new RuntimeException("Nombre es requerido");
        }
        
        if (puesto.getSalarioBase()<=0) {
            throw new RuntimeException("Salario base debe ser mayor a 0");
        }
        
        new PuestoDAO().insertar(puesto);
    }

    public LinkedList<Puesto> cargar(Area area) {     
        return new PuestoDAO().cargar(area);
    }
}
