/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg07.bienesservicios.bo;

import pkg07.bienesservicios.entities.Empleado;
import java.util.LinkedList;

/**
 *
 * @author amurilloa
 */
public class Logica {
    
    private LinkedList<Empleado> empleados;

    
    public Logica() {
    
        empleados = new LinkedList<>();
        
        empleados.add(new Empleado(1, "206470762", "Allan", "Murillo", "Alfaro"));
        empleados.add(new Empleado(1, "206520946", "Lineth", "Matamoros", "Fernández"));
        System.out.println(empleados.get(0));
        System.out.println(empleados.get(1));
    }

    public LinkedList<Empleado> getEmpleados() {
        return empleados;
    }
    
    
    
    
}
