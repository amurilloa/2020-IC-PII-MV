/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg07.bienesservicios.bo;

import pkg07.bienesservicios.dao.UsuarioDAO;
import pkg07.bienesservicios.entities.Usuario;

/**
 *
 * @author amurilloa
 */
public class UsuarioBO {

    public boolean registrar(Usuario usuario, String repass) {

        if (usuario == null) {
            throw new RuntimeException("Debe crear un nuevo usuario");
        }

        if (usuario.getUsuario().isBlank()) {
            throw new RuntimeException("Usuario requerido");
        }

        if (usuario.getCorreo().isBlank()) {
            throw new RuntimeException("Correo requerido");
        }

        if (usuario.getContrasena().length() < 8) {
            throw new RuntimeException("Contraseña con 8 caracteres como mínimo");
        }

        if (!usuario.getContrasena().equals(repass)) {
            throw new RuntimeException("Las contraseñas deben ser iguales");
        }
        //TODO: Encriptar la contraseña

        return new UsuarioDAO().insertar(usuario);
    }

    public Usuario login(Usuario usuario) {
        if (usuario == null) {
            throw new RuntimeException("Debe crear un nuevo usuario");
        }

        if (usuario.getUsuario().isBlank()) {
            throw new RuntimeException("Usuario/Correo requerido");
        }
        
        if (usuario.getContrasena().length() < 8) {
            throw new RuntimeException("Contraseña con 8 caracteres como mínimo");
        }
        return new UsuarioDAO().auntenticar(usuario);
    }

}
