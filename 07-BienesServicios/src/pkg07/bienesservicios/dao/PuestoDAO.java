/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg07.bienesservicios.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import pkg07.bienesservicios.entities.Area;
import pkg07.bienesservicios.entities.Puesto;

/**
 *
 * @author amurilloa
 */
public class PuestoDAO {

    public void insertar(Puesto puesto) {
        try ( Connection con = Conexion.getConexion()) {
            String sql = "INSERT INTO bs.puestos(id_area, codigo, nombre,salario_base, activo) VALUES (?, ?, ?, ?, ?)";
            PreparedStatement stm = con.prepareStatement(sql);
            stm.setInt(1, puesto.getArea().getId());
            stm.setString(2, puesto.getCodigo());
            stm.setString(3, puesto.getNombre());
            stm.setDouble(4, puesto.getSalarioBase());
            stm.setBoolean(5, puesto.isActivo());
            stm.execute();

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            String msj = ex.getMessage().contains("unq_area_cod")
                    ? "Código previamente registrado para esta área"
                    : "Problemas al registrar el Puesto";
            throw new RuntimeException(msj);
        }
    }

    public LinkedList<Puesto> cargar(Area area) {
        LinkedList<Puesto> puestos = new LinkedList<>();

        try ( Connection con = Conexion.getConexion()) {
            String sql = "SELECT id, id_area,  codigo, nombre, salario_base, activo FROM bs.puestos where id_area = ?";
            PreparedStatement stm = con.prepareStatement(sql);
            stm.setInt(1, area.getId());
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                puestos.add(cargar(rs));
            }

        } catch (Exception ex) {
            
            throw new RuntimeException("Favor intente nuevamente");
        }

        return puestos;
    }

    private Puesto cargar(ResultSet rs) throws SQLException {
        Puesto p = new Puesto();
        p.setId(rs.getInt(1));
        p.setArea(new AreaDAO().cargarID(rs.getInt(2)));
        p.setCodigo(rs.getString(3));
        p.setNombre(rs.getString(4));
        p.setSalarioBase(rs.getDouble(5));
        p.setActivo(rs.getBoolean(6));
        return p;

    }
}
