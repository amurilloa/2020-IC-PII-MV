/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg07.bienesservicios.dao;

import pkg07.bienesservicios.entities.Area;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 *
 * @author amurilloa
 */
public class AreaDAO {

    public void insertar(Area area) {
        try ( Connection con = Conexion.getConexion()) {
            String sql = "INSERT INTO bs.areas(codigo, nombre, activo) VALUES (?, ?, ?)";
            PreparedStatement stm = con.prepareStatement(sql);
            stm.setString(1, area.getCodigo());
            stm.setString(2, area.getNombre());
            stm.setBoolean(3, area.isActivo());
            stm.execute();

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            String msj = ex.getMessage().contains("areas_codigo_key")
                    ? "Código previamente registrado"
                    : "Problemas al registrar el Área";
            throw new RuntimeException(msj);
        }
    }

    public LinkedList<Area> cargar() {
        LinkedList<Area> areas = new LinkedList<>();

        try ( Connection con = Conexion.getConexion()) {
            String sql = "SELECT id, codigo, nombre, activo FROM bs.areas";
            PreparedStatement stm = con.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                areas.add(cargar(rs));
            }

        } catch (Exception ex) {
            throw new RuntimeException("Favor intente nuevamente");
        }

        return areas;
    }

    public Area cargarID(int id) {
        try ( Connection con = Conexion.getConexion()) {
            String sql = "SELECT id, codigo, nombre, activo FROM bs.areas where id = ? ";
            PreparedStatement stm = con.prepareStatement(sql);
            stm.setInt(1, id);
            ResultSet rs = stm.executeQuery();

            if (rs.next()) {
               return cargar(rs);
            }

        } catch (Exception ex) {
            throw new RuntimeException("Favor intente nuevamente");
        }
        return null;
    }

    private Area cargar(ResultSet rs) throws SQLException {
        Area a = new Area();
        a.setId(rs.getInt(1));
        a.setCodigo(rs.getString(2));
        a.setNombre(rs.getString(3));
        a.setActivo(rs.getBoolean(4));
        return a;

    }
}
