/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg07.bienesservicios.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import pkg07.bienesservicios.entities.Usuario;

/**
 *
 * @author amurilloa
 */
public class UsuarioDAO {

    public boolean insertar(Usuario usuario) {
        try ( Connection con = Conexion.getConexion()) {
            String sql = "INSERT INTO  bs.usuarios(usuario, correo, contrasena) VALUES (?, ?, ?)";
            PreparedStatement stm = con.prepareStatement(sql);
            stm.setString(1, usuario.getUsuario());
            stm.setString(2, usuario.getCorreo());
            stm.setString(3, usuario.getContrasena());

            return stm.executeUpdate() == 1;

        } catch (Exception ex) {
            String msj = ex.getMessage().contains("unq_usuarios_usuario")
                    ? "Usuario previamente registrado"
                    : ex.getMessage().contains("usuarios_correo_key")
                    ? "Correo previamente registrado"
                    : "Problemas al registrar el Usuario";
            throw new RuntimeException(msj);
        }

    }

    public Usuario auntenticar(Usuario usuario) {
        try ( Connection con = Conexion.getConexion()) {
            String sql = "select id, usuario, correo, contrasena, activo from bs.usuarios "
                    + " where activo = true and contrasena = ? "
                    + " and (usuario = ? or correo = ?)";
            PreparedStatement stm = con.prepareStatement(sql);
            stm.setString(1, usuario.getContrasena());
            stm.setString(2, usuario.getUsuario());
            stm.setString(3, usuario.getUsuario());
            
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                return cargar(rs);
            }

        } catch (Exception ex) {
            throw new RuntimeException("Favor intente nuevamente");
        }
        return null;
    }

    private Usuario cargar(ResultSet rs) throws SQLException {
        Usuario u = new Usuario();
        u.setId(rs.getInt(1));
        u.setUsuario(rs.getString(2));
        u.setCorreo(rs.getString(3));
        u.setContrasena(rs.getString(4));
        u.setActivo(rs.getBoolean(5));
        return u;
    }

}
