/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg07.bienesservicios.entities;

/**
 *
 * @author amurilloa
 */
public class Empleado {

    private int id;
    private String cedula;
    private String nombre;
    private String apellidoUno;
    private String apellidoDos;

    public Empleado() {
    }

    public Empleado(int id, String cedula, String nombre, String apellidoUno, String apellidoDos) {
        this.id = id;
        this.cedula = cedula;
        this.nombre = nombre;
        this.apellidoUno = apellidoUno;
        this.apellidoDos = apellidoDos;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoUno() {
        return apellidoUno;
    }

    public void setApellidoUno(String apellidoUno) {
        this.apellidoUno = apellidoUno;
    }

    public String getApellidoDos() {
        return apellidoDos;
    }

    public void setApellidoDos(String apellidoDos) {
        this.apellidoDos = apellidoDos;
    }

    @Override
    public String toString() {
        return String.format("%s -  %s %s %s", cedula, nombre, apellidoUno, apellidoDos);
    }

}
