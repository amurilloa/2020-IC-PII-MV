/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg07.bienesservicios.entities;

/**
 *
 * @author amurilloa
 */
public class Puesto {
    private int id;
    private Area area;
    private String codigo;
    private String nombre;
    private double salarioBase;
    private boolean activo;

    public Puesto() {
    }

    public Puesto(int id, Area area, String codigo, String nombre, double salarioBase, boolean activo) {
        this.id = id;
        this.area = area;
        this.codigo = codigo;
        this.nombre = nombre;
        this.salarioBase = salarioBase;
        this.activo = activo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getSalarioBase() {
        return salarioBase;
    }

    public void setSalarioBase(double salarioBase) {
        this.salarioBase = salarioBase;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    @Override
    public String toString() {
        return codigo + " - " + nombre;
    }
    
    
    
}
