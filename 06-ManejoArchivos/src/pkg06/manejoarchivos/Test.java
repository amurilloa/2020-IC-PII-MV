/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg06.manejoarchivos;

import java.util.LinkedList;

/**
 *
 * @author amurilloa
 */
public class Test {

    public static void main(String[] args) {
        Logica log = new Logica();
        System.out.println(log.getPersonas());
        
        Persona p = new Persona(206470769, "Pedro", "Rosales");
        System.out.println(log.registrar(p));

        System.out.println(log.getPersonas());
        
    }

}
