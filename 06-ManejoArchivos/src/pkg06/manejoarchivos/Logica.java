/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg06.manejoarchivos;

import java.util.LinkedList;

/**
 *
 * @author amurilloa
 */
public class Logica {

    private LinkedList<Persona> personas;
    private ManejoArchivos ma;
    private final String RUTA_P = "datos.txt";

    public Logica() {
        personas = new LinkedList<>();
        ma = new ManejoArchivos();
        cargarBD();
    }

    public boolean registrar(Persona p) {
        for (Persona persona : personas) {
            if (persona != null && persona.getCedula() == p.getCedula()) {
                return false;
            }
        }
        personas.add(p);
        guardarBD();
        return true;
    }

    public String getPersonas() {
        String txt = "Lista de Personas\n";
        int i = 1;
        for (Persona persona : personas) {
            if (persona != null) {
                txt += i++ + ". " + persona + "\n";
            }
        }
        return txt;
    }

    private void cargarBD() {
        String lectura = ma.leer(RUTA_P).trim();
        System.out.println(lectura);
        String filas[] = lectura.split("\n");
        for (String fila : filas) {
            personas.add(new Persona(fila));
        }
    }

    private void guardarBD() {
        String data = "";
        for (Persona persona : personas) {
            data += persona.getData() + "\n";
        }
        System.out.println(personas.size());
        System.out.println(data);
        ma.escribir(RUTA_P, data.trim());
    }

}
