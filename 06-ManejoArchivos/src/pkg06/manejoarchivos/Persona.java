/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg06.manejoarchivos;

/**
 *
 * @author amurilloa
 */
public class Persona {

    private int cedula;
    private String nombre;
    private String apellido;

    public Persona() {
    }

    public Persona(int cedula, String nombre, String apellido) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.apellido = apellido;
    }

    public Persona(String fila) {
        String[] datos = fila.split(",");
        cedula = Integer.parseInt(datos[0]);
        nombre = datos[1];
        apellido = datos[2];
    }

    public int getCedula() {
        return cedula;
    }

    public void setCedula(int cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getData() {
        return String.format("%d,%s,%s", cedula, nombre, apellido);
    }

    @Override
    public String toString() {
        return cedula + " - " + nombre + " " + apellido;
    }

}
