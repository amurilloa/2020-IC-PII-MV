/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplorelaciones;

/**
 *
 * @author ALLAN
 */
public class Corazon {

    private int ritmo;

    public Corazon() {
        ritmo = 90;
    }
    
    public int leerRitmo() {
        return ritmo;
    }

    public void cambiaRitmo(int ritmo) {
        this.ritmo = ritmo;
    }

    @Override
    public String toString() {
        return "Corazon{" + "ritmo=" + ritmo + '}';
    }
}
