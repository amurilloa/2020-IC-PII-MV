/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplorelaciones;

/**
 *
 * @author ALLAN
 */
public class Persona {

    private String nombre;
    private Corazon corazon;
    private Coche coche;

    public Persona(String nombre) {
        corazon = new Corazon();
        this.nombre = nombre;
    }

    public void asignaCoche(Coche coche) {
        this.coche = coche;
    }

    public void viaja() {
    }

    public void emociona() {
        corazon.cambiaRitmo(corazon.leerRitmo() + 10);
    }

    public void tranquiliza() {
        corazon.cambiaRitmo(corazon.leerRitmo() - 10);
    }

    public String getNombre() {
        return nombre;
    }

    @Override
    public String toString() {
        return "Persona{" + "nombre=" + nombre + ", corazon=" + corazon + ", coche=" + coche + '}';
    }

}
