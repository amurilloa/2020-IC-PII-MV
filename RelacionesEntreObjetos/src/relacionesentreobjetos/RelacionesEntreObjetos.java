/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package relacionesentreobjetos;

import java.time.LocalDate;

/**
 *
 * @author ALLAN
 */
public class RelacionesEntreObjetos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Cliente cli = new Cliente("Allan", "Murillo Alfaro",
                "50m este de la Escuela Procopio Gamboa", "Chachagua", LocalDate.parse("1988-06-28"));

        Cliente cli2 = new Cliente("Roberto", "Murillo Alfaro",
                "50m este de la Escuela El Bosque", "Bosque", LocalDate.parse("1987-03-03"));

        System.out.println(cli);
        System.out.println(cli.nombreCompleto());
        System.out.println(cli.direccionCompleta());

        Cuenta c1 = new Cuenta();
        c1.setTitular(cli);
        Cuenta c2 = new Cuenta(1231546, cli, 2);
        cli.setLocalidad("Fortuna");

        System.out.println(c2);
        System.out.println(c2.getNumero());
        System.out.println(c2.getTitular().nombreCompleto());
        System.out.println(c2.getTitular().direccionCompleta());
        c2.ingreso(1000);
        c2.ingreso(2000);
        c2.reintegro(500);
        c2.reintegro(400);
        c2.ingreso(1500);
        System.out.println(c2);

    }

}
