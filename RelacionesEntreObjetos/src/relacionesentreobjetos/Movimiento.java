/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package relacionesentreobjetos;

import java.time.LocalDate;

/**
 *
 * @author ALLAN
 */
public class Movimiento {

    private final LocalDate fecha;
    private final char tipo;
    private final float importe;
    private final float saldo;

    public Movimiento(LocalDate fecha, char tipo, float importe, float saldo) {
        this.fecha = fecha;
        this.tipo = tipo;
        this.importe = importe;
        this.saldo = saldo;
    }

    @Override
    public String toString() {
        return "Movimiento{" + "fecha=" + fecha + ", tipo=" + tipo + ", importe=" + importe + ", saldo=" + saldo + '}';
    }
}
