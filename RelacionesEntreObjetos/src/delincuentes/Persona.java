/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package delincuentes;

import java.util.LinkedList;

/**
 *
 * @author ALLAN
 */
public class Persona {

    private String nombre;
    private String apellido;

    private Foto foto;
    private LinkedList<Perfil> perfiles;
    private LinkedList<Lugar> lugares;

    public Persona() {
        perfiles = new LinkedList<>();
        lugares = new LinkedList<>();
    }

    public Persona(String nombre, String apellido, Foto foto, String perfilesP) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.foto = foto;
        perfiles = new LinkedList<>();
        lugares = new LinkedList<>();
        procesar(perfilesP);
    }

    private void procesar(String perfiles){
        String[] datos = perfiles.split(",");
        for (String dato : datos) {
            this.perfiles.add(new Perfil(this.perfiles.size()+1, dato));
        }
    }
    
    public void agregarLugar(Lugar lugar) {
        lugares.add(lugar);
    }
    
    public void eliminarLugar(Lugar lugar) {
        lugares.remove(lugar);
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Foto getFoto() {
        return foto;
    }

    public void setFoto(Foto foto) {
        this.foto = foto;
    }

    public LinkedList<Lugar> getLugares() {
        return lugares;
    }

    public void setLugares(LinkedList<Lugar> lugares) {
        this.lugares = lugares;
    }

    public LinkedList<Perfil> getPerfiles() {
        return perfiles;
    }

    @Override
    public String toString() {
        return "Persona{" + "nombre=" + nombre + ", apellido=" + apellido + ", foto=" + foto + ", perfiles=" + perfiles + ", lugares=" + lugares + '}';
    }

}
