/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package delincuentes;

import java.awt.Image;
import javax.swing.ImageIcon;

/**
 *
 * @author ALLAN
 */
public class Util {

    public static ImageIcon cargarFoto(int ancho, int alto, String ruta) {
        ImageIcon image = new ImageIcon(ruta);
        Image newImage = image.getImage().getScaledInstance(ancho, alto, Image.SCALE_SMOOTH);
        return new ImageIcon(newImage);
    }
}
