/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package delincuentes;

import java.util.LinkedList;

/**
 *
 * @author ALLAN
 */
public class Logica {

    private LinkedList<Persona> personas;
    private LinkedList<Lugar> lugares;

    public Logica() {
        personas = new LinkedList<>();
        lugares = new LinkedList<>();
        cargarLugares();
        personas.add(new Persona("Allan", "Murillo", new Foto("c:/Foto.png", "png"), "Ladron, Asesino"));
        personas.add(new Persona("Luis", "Mora", new Foto("c:/Foto.png", "png"), "Ladron, Asesino"));
    }

    private void cargarLugares() {
        lugares.add(new Lugar("Parque Fortuna", "Fortuna Centro", ""));
        lugares.add(new Lugar("Super Cristian #1", "200m norte la iglesia de la Fortuna", "2479-1796"));
        lugares.add(new Lugar("Florencia", "Florencia", ""));
        lugares.add(new Lugar("Puente Casa", "300m norte de la Escuela de Puente Casa", ""));
        lugares.add(new Lugar("Cancha de Futbol", "Santa Clara", ""));
    }

    public void registrarPersona(Persona p) {
        personas.add(p);
    }

    public LinkedList<Persona> getPersonas() {
        return personas;
    }

    public LinkedList<Lugar> getLugares() {
        return lugares;
    }
    
    

}
