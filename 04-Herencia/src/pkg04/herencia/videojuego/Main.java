/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg04.herencia.videojuego;

/**
 *
 * @author ALLAN
 */
public class Main {

    public static void main(String[] args) {
        Mago m1 = new Mago("Quemar", "Harry Potter");
        Guerrero g1 = new Guerrero("Hacha", "AXE", 70);
        System.out.println(m1);
        m1.encantar();
        System.out.println(m1);
        System.out.println(g1);
        g1.combatir(15);
        System.out.println(g1);
        g1.alimentarse(50);
        System.out.println(g1);
    }
}
