/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg04.herencia.vehiculo;

/**
 *
 * @author ALLAN
 */
public class CamionCompartimientos extends Camion {

    private int compartimientos;

    public CamionCompartimientos() {
    }

    public CamionCompartimientos(int compartimientos, int toneladas, String placa, String marca) {
        super(toneladas, placa, marca);
        this.compartimientos = compartimientos;
    }
    
    public double capacidadCompartimiento() {
        return (double) toneladas / compartimientos;
    }

    public String descripcion() {
        return getMarca() + " " + compartimientos;
    }

    public int getCompartimientos() {
        return compartimientos;
    }

    public void setCompartimientos(int compartimientos) {
        this.compartimientos = compartimientos;
    }
    
    @Override
    public String toString() {
        return "CamionCompartimientos{" + "compartimientos=" + compartimientos + '}';
    }

}
