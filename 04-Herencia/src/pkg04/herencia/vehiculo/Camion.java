/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg04.herencia.vehiculo;

/**
 *
 * @author ALLAN
 */
public class Camion extends Vehiculo {

    public int toneladas;

    public Camion() {
    }

    public Camion(int toneladas, String placa, String marca) {
        super(placa, marca);
        this.toneladas = toneladas;
    }

    public int getToneladas() {
        return toneladas;
    }

    public void setToneladas(int toneladas) {
        this.toneladas = toneladas;
    }

    @Override
    public String toString() {
        return super.toString() + "Camion{" + "toneladas=" + toneladas + '}';
    }

}
